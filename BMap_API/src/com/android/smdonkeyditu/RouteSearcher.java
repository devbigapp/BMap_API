package com.android.smdonkeyditu;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKBusLineResult;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKPoiInfo;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.MKSuggestionResult;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;


import com.baidu.mapapi.RouteOverlay;



public class RouteSearcher extends Activity {
	Button mBtnSearch = null;	// 搜索按钮
	MKSearch mSearch = null;	// 搜索模块，也可去掉地图模块独立使用
	String  mCityName = null;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.routesearcher);
        // 初始化搜索模块，注册事件监听
        mSearch = new MKSearch();
        mSearch.init(smdonkeyditu.mBMapMan, new MKSearchListener(){

			public void onGetPoiResult(MKPoiResult res, int type, int error) {
				// 错误号可参考MKEvent中的定义
				if (error != 0 || res == null) {
					Toast.makeText(RouteSearcher.this, "抱歉，未找到结果", Toast.LENGTH_LONG).show();
					return;
		        }
				// 找到公交路线POI node
                MKPoiInfo curPoi = null;
                int totalPoiNum  = res.getNumPois();
				for( int idx = 0; idx < totalPoiNum; idx++ ) {
					Log.d("busline", "the busline is " + idx);
                    curPoi = res.getPoi(idx);
                    if ( 2 == curPoi.ePoiType ) {
                    	break;
                    }
				}

				mSearch.busLineSearch(mCityName, curPoi.uid);
			}
			public void onGetDrivingRouteResult(MKDrivingRouteResult res,
					int error) {
			}
			public void onGetTransitRouteResult(MKTransitRouteResult res,
					int error) {
			}
			public void onGetWalkingRouteResult(MKWalkingRouteResult res,
					int error) {
			}
			public void onGetAddrResult(MKAddrInfo res, int error) {
				smdonkeyditu.mMapView.getController().animateTo(res.geoPt);	
			}
			public void onGetBusDetailResult(MKBusLineResult result, int iError) {
				if (iError != 0 || result == null) {
					Toast.makeText(RouteSearcher.this, "抱歉，未找到结果", Toast.LENGTH_LONG).show();
					return;
		        }

				RouteOverlay routeOverlay = new RouteOverlay(RouteSearcher.this, smdonkeyditu.mMapView);
			    // 此处仅展示一个方案作为示例
			    routeOverlay.setData(result.getBusRoute());
			    smdonkeyditu.mMapView.getOverlays().clear();
			    smdonkeyditu.mMapView.getOverlays().add(routeOverlay);
			    smdonkeyditu.mMapView.invalidate();
			    
			    smdonkeyditu.mMapView.getController().animateTo(result.getBusRoute().getStart());
			}
			@Override
			public void onGetSuggestionResult(MKSuggestionResult res, int arg1) {
				// TODO Auto-generated method stub
				
			}
			

        });        
        // 设定搜索按钮的响应
        mBtnSearch = (Button)findViewById(R.id.searchBusLine);
        OnClickListener clickListener3 = new OnClickListener(){
			public void onClick(View v) {
				if (mBtnSearch.equals(v)) {
					finish();
					EditText editCity = (EditText)findViewById(R.id.buslinecity);
					EditText editSearchKey = (EditText)findViewById(R.id.buslinesearchkey);
					mCityName = editCity.getText().toString(); 
					mSearch.poiSearchInCity(mCityName, editSearchKey.getText().toString());
					
				}
			}
        };
        
        mBtnSearch.setOnClickListener(clickListener3); 
	}	
	

}