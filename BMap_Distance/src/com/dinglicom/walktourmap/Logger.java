/*
 * 文件名: Logger.java
 * 版    权：  Copyright Etop Group  All Rights Reserved.
 * 描    述: [该类的简要描述]
 * 创建人: 黄广府
 * 创建时间:Apr 13, 2011
 *
 * 修改人：
 * 修改时间:
 * 修改内容：[修改内容]
 */
package com.dinglicom.walktourmap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import android.os.Environment;
import android.util.Log;

/**
 * 日志的功能操作类 可将日志保存至SD卡
 *
 * @author 黄广府
 * @version [Android RCS C02, 2011-04-13]
 */
public class Logger {
    
    /**
     * DEBUG级别开关
     */
    private static final boolean DEBUG = true;
    
    /**
     * 是否保存至SD卡
     */
    private static final boolean SAVE_TO_SD = false;
    
    /**
     * 保存LOG日志的目录
     */
    private static final String SAVE_LOG_DIR_PATH = Environment.getExternalStorageDirectory()
            .getPath()
            + "/Walktour";
    
    /**
     * 保存LOG日志的路径
     */
    private static final String SAVE_LOG_PATH = Environment.getExternalStorageDirectory()
            .getPath()
            + "/Walktour/log.txt";
    
    /**
     * 日志打印时间Format
     */
    private static final SimpleDateFormat fmt = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    
    /**
     * 用于打印错误级的日志信息
     * @param strModule LOG TAG
     * @param strErrMsg 打印信息
     */
    public static void e(String strModule, String strErrMsg) {
        if (DEBUG) {
            Log.e(strModule, ">>" + strErrMsg + "<<");
            if (SAVE_TO_SD) {
                storeLog(strModule, strErrMsg);
            }
        }
    }
    
    /**
     * 用于打印描述级的日志信息
     * @param strModule LOG TAG
     * @param strErrMsg 打印信息
     */
    public static void i(String strModule, String strErrMsg) {
        if (DEBUG) {
            Log.i(strModule, ">>" + strErrMsg + "<<");
            if (SAVE_TO_SD) {
                storeLog(strModule, strErrMsg);
            }
        }
    }
    
    /**
     * 用于打印描述级的日志信息
     * @param strModule LOG TAG
     * @param strErrMsg 打印信息
     */
    public static void d(String strModule, String strErrMsg) {
        if (DEBUG) {
            Log.d(strModule, ">>" + strErrMsg + "<<");
            if (SAVE_TO_SD) {
                storeLog(strModule, strErrMsg);
            }
        }
    }
    
    /**
     * 将日志信息保存至SD卡
     * @param strModule LOG TAG
     * @param strErrMsg 保存的打印信息
     */
    public static void storeLog(String strModule, String strErrMsg) {
        if (Environment.getExternalStorageState()
                .equals(Environment.MEDIA_MOUNTED)) {
            File fileDir = new File(SAVE_LOG_DIR_PATH);
            // 判断目录是否已经存在
            if (!fileDir.exists()) {
                if (!fileDir.mkdir()) {
                    Log.e(strModule, "Failed to create directory "
                            + SAVE_LOG_DIR_PATH);
                    return;
                }
            }
            File file = new File(SAVE_LOG_PATH);
            // 判断日志文件是否已经存在
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                // 输出
                FileOutputStream fos = new FileOutputStream(file, true);
                PrintWriter out = new PrintWriter(fos);
                synchronized (fmt) {
                    out.println(fmt.format(System.currentTimeMillis()) + "  >>"
                            + strModule + "<<  " + strErrMsg + '\r');
                }
                out.flush();
                out.close();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * 获取DEBUG状态
     * @return
     */
    public static boolean isDebuggable() {
        return DEBUG;
    }
}
