/*
 * 文件名: City.java
 * 版    权：  Copyright DingliCom Tech. Co. Ltd. All Rights Reserved.
 * 描    述: [该类的简要描述]
 * 创建人: 黄广府
 * 创建时间:2012-5-31
 * 
 * 修改人：
 * 修改时间:
 * 修改内容：[修改内容]
 */
package com.dinglicom.walktourmap.logic.model;

import java.io.Serializable;

/**
 * [一句话功能简述]<BR>
 * [功能详细描述]
 * @author 黄广府
 * @version [WalkTour Client V100R001C03, 2012-5-31] 
 */
public class City implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = -1235645888499576488L;

    private int id;
    
    /**
     * 城市名称
     */
    private String name;
    
    /**
     * 城市地图大小
     */
    private int size;
    
    /**
     * 拼音
     */
    private String spellName;
    
    /**
     * 拼音简称
     */
    private String initialName;
    
    private String simplePinyin;
    
    private String nameHeadLetter;

    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the spellName
     */
    public String getSpellName() {
        return spellName;
    }

    /**
     * @param spellName the spellName to set
     */
    public void setSpellName(String spellName) {
        this.spellName = spellName;
    }

    /**
     * @return the initialName
     */
    public String getInitialName() {
        return initialName;
    }

    /**
     * @param initialName the initialName to set
     */
    public void setInitialName(String initialName) {
        this.initialName = initialName;
    }

    /**
     * @return the simplePinyin
     */
    public String getSimplePinyin() {
        return simplePinyin;
    }

    /**
     * @param simplePinyin the simplePinyin to set
     */
    public void setSimplePinyin(String simplePinyin) {
        this.simplePinyin = simplePinyin;
    }

    /**
     * @return the nameHeadLetter
     */
    public String getNameHeadLetter() {
        return nameHeadLetter;
    }

    /**
     * @param nameHeadLetter the nameHeadLetter to set
     */
    public void setNameHeadLetter(String nameHeadLetter) {
        this.nameHeadLetter = nameHeadLetter;
    }

    /**
     * [一句话功能简述]<BR>
     * [功能详细描述]
     * @return
     * @see java.lang.Object#toString()
     */
    
    @Override
    public String toString() {
        return "City [id=" + id + ", name=" + name + ", size=" + size
                + ", spellName=" + spellName + ", initialName=" + initialName
                + ", simplePinyin=" + simplePinyin + ", nameHeadLetter="
                + nameHeadLetter + "]";
    }
    
    
}

