package com.baidu.mapapi.demo.ui;

import android.os.Bundle;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-8 上午9:57:08
 * @annotation 第一个例子：加载百度地图
 */
public class BMap_Hello extends MapActivity {

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_map);
		setTitle("Hello BMap");

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();

		this.initMapActivity(app.mBMapManager);// 初始化

		MapView mapView = (MapView) findViewById(R.id.bmapView);
		
		mapView.setBuiltInZoomControls(true);// 允许缩放操作
		mapView.getController().setZoom(12);// 设置缩放级别为12
		mapView.setDoubleClickZooming(true);//允许双击放大
		mapView.setTraffic(true);// 显示交通图层
		mapView.setSatellite(true);// 显示卫星图
	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager != null)
			app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	// @Override
	protected void onDestroy() {

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager != null) {
			app.mBMapManager.destroy();
			app.mBMapManager = null;
		}
		super.onDestroy();
	}
}
