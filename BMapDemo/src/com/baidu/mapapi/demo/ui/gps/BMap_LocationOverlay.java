package com.baidu.mapapi.demo.ui.gps;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.LocationListener;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.MyLocationOverlay;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-8 上午10:01:48
 * @annotation 定位图层
 */
public class BMap_LocationOverlay extends MapActivity {

	MapView mMapView = null;
	LocationListener mLocationListener = null;// onResume时注册此listener，onPause时需要Remove

	MyLocationOverlay mLocationOverlay = null; // 定位图层

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_map);
		setTitle("GPS定位");

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();

		super.initMapActivity(app.mBMapManager); // 如果使用地图SDK，请初始化地图Activity

		mMapView = (MapView) findViewById(R.id.bmapView);

		mMapView.setBuiltInZoomControls(true);

		mMapView.setDrawOverlayWhenZooming(true); // 设置在缩放动画过程中也显示overlay,默认为不绘制

		mLocationOverlay = new MyLocationOverlay(this, mMapView);// 初始化定位图层
		mMapView.getOverlays().add(mLocationOverlay);// 添加定位图层

		// 注册定位事件
		mLocationListener = new LocationListener() {

			@Override
			public void onLocationChanged(Location location) {
				if (location != null) {
					double x = location.getLatitude();
					double y = location.getLongitude();
					String xy = "GPS坐标：(" + x + "," + y + ")";
					Log.i("andli", xy);
					Toast.makeText(BMap_LocationOverlay.this, xy, 5000).show();
					GeoPoint pt = new GeoPoint(
							(int) (location.getLatitude() * 1e6),
							(int) (location.getLongitude() * 1e6));
					mMapView.getController().animateTo(pt);// 对以给定的点GeoPoint，开始动画显示地图。
				}
			}
		};
	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.getLocationManager().removeUpdates(mLocationListener);// 移除位置监听listener
		mLocationOverlay.disableMyLocation();// 停止位置更新。
		mLocationOverlay.disableCompass(); // 关闭指南针的更新。
		app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();

		app.mBMapManager.getLocationManager().requestLocationUpdates(mLocationListener);// 注册定位事件，定位后将地图移动到定位点

		mLocationOverlay.enableMyLocation();// 尝试开启MyLocation功能，并向MKLocationManager.GPS_PROVIDER和MKLocationManager.NETWORK_PROVIDER注册更新。
		mLocationOverlay.enableCompass(); // 打开指南针

		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

}
