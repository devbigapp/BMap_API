package com.baidu.mapapi.demo.ui.my;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKBusLineResult;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKPoiInfo;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.MKSuggestionResult;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-18 上午9:14:29
 * @annotation 本地搜索图层：POI兴趣点(分页检索)
 */
public class BMap_PoiSearch extends MapActivity implements OnClickListener {
	FrameParam app = null;

	private Button searchButton = null; // 搜索按钮

	private TextView resultTextView;

	private EditText cityNamEditText;
	private EditText cityKeyEditText;

	private ListView resListView = null;

	MKSearch mMkSearch = null; // 搜索模块，也可去掉地图模块独立使用
	private static List<MKPoiInfo> poiList;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_mypoisearch);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setTitle("我的周边");
		initUI();
		search();
	}

	private void initUI() {
		app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();
		// super.initMapActivity(app.mBMapManager);去掉，否则报错，没有加载地图，无需初始化

		resListView = (ListView) findViewById(R.id.listView1);
		searchButton = (Button) findViewById(R.id.search);
		searchButton.setOnClickListener(this);

		cityNamEditText = (EditText) findViewById(R.id.city);
		cityKeyEditText = (EditText) findViewById(R.id.searchkey);

		resultTextView = (TextView) findViewById(R.id.result);

		cityKeyEditText.setText("川菜");

		// 设置每页返回的POI数，默认为10，取值范围1-50
		MKSearch.setPoiPageCapacity(10);
		// 初始化搜索模块，注册事件监听
		mMkSearch = new MKSearch();
		mMkSearch.init(app.mBMapManager, new PoiSearch());
	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onClick(View arg0) {

		if (arg0 == searchButton) {
			search();
		}

	}

	private void search() {
		poiList = new ArrayList<MKPoiInfo>();

		// 1.城市兴趣点检索
		String cityName = cityNamEditText.getText().toString();
		String key = cityKeyEditText.getText().toString();
		mMkSearch.poiSearchInCity(cityName, key);

		// 2.根据中心点、半径与检索词发起周边检索
//		double x, y;
//		x = 39.90923;
//		y = 116.397428;
//		x = app.my_x;
//		y = app.my_y;

//		GeoPoint geoPoint = new GeoPoint((int) (x * 1e6), (int) (y * 1e6));
//		// 单个检索词
//		mMkSearch.poiSearchNearBy(key, geoPoint, 1000);
//		// 多个检索词
//		String[] keys = new String[] { key, key };
//		mMkSearch.poiMultiSearchNearBy(keys, geoPoint, 200);

	

	}

	// POI搜索类
	class PoiSearch implements MKSearchListener {

		// 获的兴趣点的查询结果
		public void onGetPoiResult(MKPoiResult res, int type, int error) {

			if (error != 0 || res == null) { // 错误号可参考MKEvent中的定义
				Toast.makeText(BMap_PoiSearch.this, "抱歉，未找到结果", 5000).show();
				return;
			}
			if (res.getCurrentNumPois() > 0) {
				getPoiResult(res);
			} else {
				Toast.makeText(BMap_PoiSearch.this, "搜索结果为空！", 5000).show();
			}
		}

		public void onGetDrivingRouteResult(MKDrivingRouteResult res, int error) {
		}

		public void onGetTransitRouteResult(MKTransitRouteResult res, int error) {
		}

		public void onGetWalkingRouteResult(MKWalkingRouteResult res, int error) {
		}

		public void onGetAddrResult(MKAddrInfo res, int error) {
		}

		public void onGetBusDetailResult(MKBusLineResult result, int iError) {
		}

		@Override
		public void onGetSuggestionResult(MKSuggestionResult res, int arg1) {
		}

		@Override
		public void onGetRGCShareUrlResult(String arg0, int arg1) {

		}

	}

	public void getPoiResult(MKPoiResult res) {
		int NumPois = res.getCurrentNumPois();// 当前返回的兴趣点总数
		if (NumPois > 0) {
			int curNum = res.getCurrentNumPois();
			for (MKPoiInfo mkPoiInfo : res.getAllPoi()) {
				poiList.add(mkPoiInfo);// 累加搜索结果
			}
			// 如果当前页的索引为0，表示第一页搜索结果
			if (res.getPageIndex() == 0) {
				Log.i("andli", "第一页码=" + res.getPageIndex() + "(" + curNum
						+ ")");
			}
			// 跳转到下一页***
			if (res.getPageIndex() < res.getNumPages() - 1) {
				if (res.getPageIndex() != 0) {
					Log.i("andli", "当前页码=" + res.getPageIndex() + "(" + curNum
							+ ")");
				}
				mMkSearch.goToPoiPage(res.getPageIndex() + 1);// 查询指定页
			}
			// 最后一页，显示搜索结果
			else if (res.getPageIndex() == res.getNumPages() - 1) {
				Log.i("andli", "最后一页=" + res.getPageIndex() + "(" + curNum
						+ ")");
				Log.i("andli", "页总数=" + res.getNumPages());
				Log.i("andli", "POI总数=" + poiList.size());
				resultTextView.setText("搜索到" + poiList.size() + "个POI");

				showPoiByListView(poiList);
				// BMapHelp.showPoiByDialog(this,poiList);
			}

		}

	}

	// 通过ListView来显示搜索结果
	private void showPoiByListView(List<MKPoiInfo> _poiList) {
		String[] items = new String[_poiList.size()];
		int j = 1;
		for (int i = 0; i < items.length; i++) {
			String name = _poiList.get(i).name;
			String address = _poiList.get(i).address;
			items[i] = (j++) + "." + name + "\n" + address;
		}
		ArrayAdapter<String> adp = new ArrayAdapter<String>(
				BMap_PoiSearch.this, android.R.layout.simple_list_item_1, items);
		resListView.setAdapter(adp);

	}

}
