package com.baidu.mapapi.demo.ui.Overlay;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKBusLineResult;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKPoiInfo;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.MKSuggestionResult;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.RouteOverlay;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.BMapHelp;
import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-18 上午11:10:36
 * @annotation 公交路线详细信息搜索
 */
public class BMap_BusLineSearch extends MapActivity implements OnClickListener {
	Button mBtnSearch = null; // 搜索按钮

	MapView mMapView = null; // 地图View
	MKSearch mSearch = null; // 搜索模块，也可去掉地图模块独立使用
	String mCityName = null;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_buslinesearch);
		setTitle("公交路线详细信息搜索");

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();
		super.initMapActivity(app.mBMapManager);
		initUI();

		// 初始化搜索模块，注册事件监听
		mSearch = new MKSearch();
		mSearch.init(app.mBMapManager, new MySearch());

	}

	private void initUI() {
		mMapView = (MapView) findViewById(R.id.bmapView);
		mMapView.setBuiltInZoomControls(true);
		mMapView.setDrawOverlayWhenZooming(true);
		// 设定搜索按钮的响应
		mBtnSearch = (Button) findViewById(R.id.search);
		mBtnSearch.setOnClickListener(this);

	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	class MySearch implements MKSearchListener {

		public void onGetPoiResult(MKPoiResult res, int type, int error) {
			try {
				if (error != 0 || res == null) {// 错误号可参考MKEvent中的定义
					Toast.makeText(BMap_BusLineSearch.this, "结果为null", 2000)
							.show();
					return;
				}

				// 找到公交路线poi node
				MKPoiInfo mMkPoiInfo = null;
				MKPoiInfo busLineInfo = null;
				int numPois = res.getAllPoi().size();
				for (int index = 0; index < numPois; index++) {
					Log.i("andli", "---" + index + "---");
					mMkPoiInfo = res.getPoi(index);
					BMapHelp.showMKPoiInfo(mMkPoiInfo);
					if (2 == mMkPoiInfo.ePoiType) {
						// poi类型，0：普通点，1：公交站，2：公交线路，3：地铁站，4：地铁线路
						mSearch.busLineSearch(mCityName, mMkPoiInfo.uid);// 公交路线详细信息搜索
						busLineInfo = mMkPoiInfo; // 异步函数，返回结果在MKSearchListener里的
						break;
					}
				}

				// 没有找到公交信息
				if (busLineInfo == null) {
					Toast.makeText(BMap_BusLineSearch.this, "结果为空", 2000)
							.show();
					return;
				}
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(BMap_BusLineSearch.this, e.toString(), 10000)
						.show();
			}

		}

	

		public void onGetDrivingRouteResult(MKDrivingRouteResult res, int error) {
		}

		public void onGetTransitRouteResult(MKTransitRouteResult res, int error) {
		}

		public void onGetWalkingRouteResult(MKWalkingRouteResult res, int error) {
		}

		public void onGetAddrResult(MKAddrInfo res, int error) {
		}

		public void onGetBusDetailResult(MKBusLineResult result, int iError) {
			if (iError != 0 || result == null) {
				Toast.makeText(BMap_BusLineSearch.this, "null", 2000).show();
				return;
			}

			RouteOverlay routeOverlay = new RouteOverlay(
					BMap_BusLineSearch.this, mMapView);
			// 此处仅展示一个方案作为示例
			routeOverlay.setData(result.getBusRoute());
			mMapView.getOverlays().clear();
			mMapView.getOverlays().add(routeOverlay);
			mMapView.invalidate();

			mMapView.getController().animateTo(result.getBusRoute().getStart());
		}

		@Override
		public void onGetSuggestionResult(MKSuggestionResult res, int arg1) {

		}

		@Override
		public void onGetRGCShareUrlResult(String arg0, int arg1) {

		}

	}

	@Override
	public void onClick(View v) {
		if (v == mBtnSearch) {
			EditText editCity = (EditText) findViewById(R.id.city);
			EditText editSearchKey = (EditText) findViewById(R.id.searchkey);
			mCityName = editCity.getText().toString();
			mSearch.poiSearchInCity(mCityName, editSearchKey.getText()
					.toString());
		}

	}

}
