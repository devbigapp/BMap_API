package com.baidu.mapapi.demo.ui.Overlay;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKBusLineResult;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKPlanNode;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.MKSuggestionResult;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.RouteOverlay;
import com.baidu.mapapi.TransitOverlay;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-18 上午10:36:25
 * @annotation 步行，驾车导航线路图层，将步行，驾车出行方案的路线及关键点显示在地图上
 */
public class BMap_RouteOverlay extends MapActivity implements OnClickListener {

	Button mBtnDrive = null; // 驾车搜索
	Button mBtnTransit = null; // 公交搜索
	Button mBtnWalk = null; // 步行搜索

	MapView mMapView = null; // 地图View
	MKSearch mMKSearch = null; // 搜索模块，也可去掉地图模块独立使用

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_routeplan);
		setTitle("导航线路图层");

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();
		super.initMapActivity(app.mBMapManager);

		initUI();

		// 初始化搜索模块，注册事件监听
		mMKSearch = new MKSearch();
		mMKSearch.init(app.mBMapManager, new MySearch());

	}

	private void initUI() {
		mMapView = (MapView) findViewById(R.id.bmapView);
		mMapView.setBuiltInZoomControls(true);
		mMapView.setDrawOverlayWhenZooming(true);

		mBtnDrive = (Button) findViewById(R.id.drive);
		mBtnTransit = (Button) findViewById(R.id.transit);
		mBtnWalk = (Button) findViewById(R.id.walk);

		mBtnDrive.setOnClickListener(this);
		mBtnTransit.setOnClickListener(this);
		mBtnWalk.setOnClickListener(this);

	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onClick(View v) {

		// 处理搜索按钮响应
		EditText editSt = (EditText) findViewById(R.id.start);
		EditText editEn = (EditText) findViewById(R.id.end);

		// 对起点终点的name进行赋值，也可以直接对坐标赋值，赋值坐标则将根据坐标进行搜索
		MKPlanNode stNode = new MKPlanNode();
		stNode.name = editSt.getText().toString();
		MKPlanNode enNode = new MKPlanNode();
		enNode.name = editEn.getText().toString();

		// 实际使用中请对起点终点城市进行正确的设定
		if (mBtnDrive.equals(v)) {
			mMKSearch.drivingSearch("北京", stNode, "上海", enNode);
		} else if (mBtnTransit.equals(v)) {
			mMKSearch.transitSearch("北京", stNode, enNode);
		} else if (mBtnWalk.equals(v)) {
			mMKSearch.walkingSearch("北京", stNode, "北京", enNode);
		}

	}

	// 搜索类
	class MySearch implements MKSearchListener {
		// 返回驾乘路线搜索结果
		public void onGetDrivingRouteResult(MKDrivingRouteResult res, int error) {
			// 错误号可参考MKEvent中的定义
			if (error != 0 || res == null) {
				Toast.makeText(BMap_RouteOverlay.this, "抱歉，未找到结果", 2000).show();
				return;
			}
			Toast.makeText(BMap_RouteOverlay.this,
					"搜索到" + res.getNumPlan() + "个结果！", 5000).show();
			RouteOverlay routeOverlay = new RouteOverlay(
					BMap_RouteOverlay.this, mMapView);
			// 此处仅展示一个方案作为示例
			routeOverlay.setData(res.getPlan(0).getRoute(0));
			mMapView.getOverlays().clear();
			mMapView.getOverlays().add(routeOverlay);
			mMapView.invalidate();

			mMapView.getController().animateTo(res.getStart().pt);
		}

		// 返回公交搜索结果
		public void onGetTransitRouteResult(MKTransitRouteResult res, int error) {
			if (error != 0 || res == null) {
				Toast.makeText(BMap_RouteOverlay.this, "抱歉，未找到结果", 2000).show();
				return;
			}
			int numPlan = res.getNumPlan();
			Toast.makeText(BMap_RouteOverlay.this, "搜索到" + numPlan + "个结果！",
					5000).show();

			mMapView.getOverlays().clear();// 清空所有覆盖物图层
			// 存在多条时：先是列表的形式显示，然后点击列表显示详情
			for (int i = 0; i < numPlan; i++) {
				TransitOverlay mTransitOverlay = new TransitOverlay(
						BMap_RouteOverlay.this, mMapView);
				mTransitOverlay.setData(res.getPlan(i));
				mMapView.getOverlays().add(mTransitOverlay);
			}
			mMapView.invalidate();// 刷新

			mMapView.getController().animateTo(res.getStart().pt);
		}

		// 返回步行路线搜索结果
		public void onGetWalkingRouteResult(MKWalkingRouteResult res, int error) {
			if (error != 0 || res == null) {
				Toast.makeText(BMap_RouteOverlay.this, "抱歉，未找到结果", 2000).show();
				return;
			}
			Toast.makeText(BMap_RouteOverlay.this,
					"搜索到" + res.getNumPlan() + "个结果！", 5000).show();
			RouteOverlay routeOverlay = new RouteOverlay(
					BMap_RouteOverlay.this, mMapView);
			// 此处仅展示一个方案作为示例
			routeOverlay.setData(res.getPlan(0).getRoute(0));
			mMapView.getOverlays().clear();
			mMapView.getOverlays().add(routeOverlay);
			mMapView.invalidate();

			mMapView.getController().animateTo(res.getStart().pt);
		}

		public void onGetAddrResult(MKAddrInfo res, int error) {
		}

		public void onGetPoiResult(MKPoiResult res, int arg1, int arg2) {
		}

		public void onGetBusDetailResult(MKBusLineResult result, int iError) {
		}

		@Override
		public void onGetSuggestionResult(MKSuggestionResult res, int arg1) {

		}

		@Override
		public void onGetRGCShareUrlResult(String arg0, int arg1) {

		}
	}

}
