package com.baidu.mapapi.demo.ui.Overlay;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKBusLineResult;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKPoiInfo;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.MKSuggestionResult;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.PoiOverlay;
import com.baidu.mapapi.demo.FrameParam;
import com.baidu.mapapi.demo.BMapHelp;
import com.baidu.mapapi.demo.R;

/**
 * 
 * @author lilin
 * @date 2012-11-18 上午9:14:29
 * @annotation 本地搜索图层：POI兴趣点
 */
public class BMap_PoiOverlay extends MapActivity implements OnClickListener {
	private Button searchButton = null; // 搜索按钮
	private Button suggestionButton = null; // suggestion搜索

	private EditText cityNamEditText;
	private EditText cityKeyEditText;
	private EditText suggestionEditText;

	ListView mSuggestionListView = null;
	public static String mSuggestions[] = {};

	MapView mMapView = null; // 地图View

	MKSearch mMkSearch = null; // 搜索模块，也可去掉地图模块独立使用

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_poisearch);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setTitle("本地搜索图层：POI兴趣点");

		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager == null) {
			app.mBMapManager = new BMapManager(getApplication());
			app.mBMapManager.init(app.mapKey, new FrameParam.MyGeneralListener());
		}
		app.mBMapManager.start();
		super.initMapActivity(app.mBMapManager);

		mMapView = (MapView) findViewById(R.id.bmapView);
		mMapView.setBuiltInZoomControls(true);
		mMapView.setDrawOverlayWhenZooming(true);

		// 初始化搜索模块，注册事件监听
		mMkSearch = new MKSearch();
		mMkSearch.init(app.mBMapManager, new PoiSearch());

		initUI();

	}

	private void initUI() {
		mSuggestionListView = (ListView) findViewById(R.id.listView1);
		searchButton = (Button) findViewById(R.id.search);
		searchButton.setOnClickListener(this);

		suggestionButton = (Button) findViewById(R.id.suggestionsearch);
		suggestionButton.setOnClickListener(this);

		cityNamEditText = (EditText) findViewById(R.id.city);
		cityKeyEditText = (EditText) findViewById(R.id.searchkey);
		suggestionEditText = (EditText) findViewById(R.id.suggestionkey);
	}

	@Override
	protected void onPause() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		app.mBMapManager.start();
		super.onResume();
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onClick(View arg0) {

		if (arg0 == searchButton) {
			// 城市poi检索
			String cityName = cityNamEditText.getText().toString();
			String key = cityKeyEditText.getText().toString();
			mMkSearch.poiSearchInCity(cityName, key);
		} else if (arg0 == suggestionButton) {
			// 联想词检索
			String suggestion = suggestionEditText.getText().toString();
			mMkSearch.suggestionSearch(suggestion);
		}

	}

	// POI搜索类
	class PoiSearch implements MKSearchListener {

		// 获的兴趣点的查询结果
		public void onGetPoiResult(MKPoiResult res, int type, int error) {

			if (error != 0 || res == null) { // 错误号可参考MKEvent中的定义
				Toast.makeText(BMap_PoiOverlay.this, "抱歉，未找到结果", 5000).show();
				return;
			}
			int NumPois = res.getCurrentNumPois();
			MKPoiInfo poiInfo;
			for (int i = 0; i < NumPois; i++) {
				poiInfo = res.getPoi(i);
				BMapHelp.showMKPoiInfo(poiInfo);
			}

			Toast.makeText(BMap_PoiOverlay.this, "搜索到" + NumPois + "个结果！", 5000)
					.show();
			// 将地图移动到第一个POI中心点
			if (NumPois > 0) {

				// 将poi结果显示到地图上
				PoiOverlay poiOverlay = new PoiOverlay(BMap_PoiOverlay.this,
						mMapView);
				poiOverlay.setData(res.getAllPoi());
				mMapView.getOverlays().clear();
				mMapView.getOverlays().add(poiOverlay);
				mMapView.invalidate();// 刷新
				mMapView.getController().animateTo(res.getPoi(0).pt);// 移到该点为中心
			}
			// else if (res.getCityListNum() > 0) {
			// String strInfo = "在";
			// for (int i = 0; i < res.getCityListNum(); i++) {
			// strInfo += res.getCityListInfo(i).city;
			// strInfo += ",";
			// }
			// strInfo += "找到结果";
			// Toast.makeText(BMap_PoiOverlay.this, strInfo, 5000).show();
			// }
		}

		public void onGetDrivingRouteResult(MKDrivingRouteResult res, int error) {
		}

		public void onGetTransitRouteResult(MKTransitRouteResult res, int error) {
		}

		public void onGetWalkingRouteResult(MKWalkingRouteResult res, int error) {
		}

		public void onGetAddrResult(MKAddrInfo res, int error) {
		}

		public void onGetBusDetailResult(MKBusLineResult result, int iError) {
		}

		// 获取关键字搜索结果，并显示到列表
		@Override
		public void onGetSuggestionResult(MKSuggestionResult res, int arg1) {
			if (arg1 != 0 || res == null) {
				Toast.makeText(BMap_PoiOverlay.this, "抱歉，未找到结果", 5000).show();
				return;
			}

			int nSize = res.getSuggestionNum();

			mSuggestions = new String[nSize];

			for (int i = 0; i < nSize; i++) {
				String city = res.getSuggestion(i).city;
				String key = res.getSuggestion(i).key;
				mSuggestions[i] = city + key;
			}

			ArrayAdapter<String> suggestionString = new ArrayAdapter<String>(
					BMap_PoiOverlay.this, android.R.layout.simple_list_item_1,
					mSuggestions);
			mSuggestionListView.setAdapter(suggestionString);

			Toast.makeText(BMap_PoiOverlay.this, "搜索到" + nSize + "个结果！", 5000)
					.show();

		}

		@Override
		public void onGetRGCShareUrlResult(String arg0, int arg1) {

		}

	}

}
