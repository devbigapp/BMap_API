package com.baidu.mapapi.demo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.baidu.mapapi.demo.ui.BMap_GeoCoder;
import com.baidu.mapapi.demo.ui.BMap_Hello;
import com.baidu.mapapi.demo.ui.BMap_InvokeMapClient;
import com.baidu.mapapi.demo.ui.BMap_Offline;
import com.baidu.mapapi.demo.ui.Overlay.BMap_BusLineSearch;
import com.baidu.mapapi.demo.ui.Overlay.BMap_ItemizedOverlay;
import com.baidu.mapapi.demo.ui.Overlay.BMap_Overlay;
import com.baidu.mapapi.demo.ui.Overlay.BMap_PoiOverlay;
import com.baidu.mapapi.demo.ui.Overlay.BMap_RouteOverlay;
import com.baidu.mapapi.demo.ui.gps.BMap_GPS_ReverseAndExtent;
import com.baidu.mapapi.demo.ui.gps.BMap_GPS_ExtentClick;
import com.baidu.mapapi.demo.ui.gps.BMap_GPS_reverseGeocode;
import com.baidu.mapapi.demo.ui.gps.BMap_LocationOverlay;
import com.baidu.mapapi.demo.ui.mark.BMap_LongPressMarkPoint;
import com.baidu.mapapi.demo.ui.my.BMap_PoiSearch;

public class BMap_MainView extends Activity {

	ListView mListView = null;
	String mStrDemos[] = { "Hello BMap(简单调用)", "GPS(反地址查询)", "GPS(反地址+500米范围查询+距离计算)",
			"GPS(500米范围可点击查询)", "我的周边", "位置标注", "MyLocationOverlay(GPS定位)",
			"Overlay(单个覆盖物)", "ItemizedOverlay(一组覆盖物)",
			"PoiOverlay(本地搜索--兴趣点)", "RouteOverlay(步行,驾车,公交导航线路图层)",
			"BusLineSearch(公交路线详细信息搜索)", "GeoCoder(坐标和地址编码转换)",
			"Offline(离线地图)", "调用百度地图客户端", "销毁地图引擎" };
	Class<?> clazzs[] = { BMap_Hello.class, BMap_GPS_reverseGeocode.class,
			BMap_GPS_ReverseAndExtent.class, BMap_GPS_ExtentClick.class,
			BMap_PoiSearch.class, BMap_LongPressMarkPoint.class,
			BMap_LocationOverlay.class, BMap_Overlay.class,
			BMap_ItemizedOverlay.class, BMap_PoiOverlay.class,
			BMap_RouteOverlay.class, BMap_BusLineSearch.class,
			BMap_GeoCoder.class, BMap_Offline.class, BMap_InvokeMapClient.class };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmap_main);

		// String versionInof = VersionInfo.getApiVersion();
		mListView = (ListView) findViewById(R.id.listView);
		// 添加ListItem，设置事件响应
		List<String> data = new ArrayList<String>();
		for (int i = 0; i < mStrDemos.length; i++) {
			data.add(mStrDemos[i]);
		}
		mListView.setAdapter((ListAdapter) new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, data));
		mListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v, int index,
					long arg3) {
				onListItemClick(index);
			}
		});
	}

	void onListItemClick(int index) {
		if (index < 0 || index >= clazzs.length + 1)
			return;

		if (index == clazzs.length) {// 退出
			FrameParam app = (FrameParam) this.getApplication();
			if (app.mBMapManager != null) {
				app.mBMapManager.destroy();
				app.mBMapManager = null;
			}
			return;
		}

		Intent intent = null;
		intent = new Intent(BMap_MainView.this, clazzs[index]);
		this.startActivity(intent);
	}

	@Override
	protected void onResume() {
		FrameParam app = (FrameParam) this.getApplication();
		if (!app.isRightKey) {
			TextView text = (TextView) findViewById(R.id.text_Info);
			text.setText("请在BMapApiDemoApp.java文件输入正确的授权Key！\r\n"
					+ "申请地址：http://dev.baidu.com/wiki/static/imap/key/");
			text.setTextColor(Color.RED);
		}
		super.onResume();
	}

	// 建议在APP整体退出之前调用MapApi的destroy()函数，不要在每个activity的OnDestroy中调用，
	// 避免MapApi重复创建初始化，提高效率
	@Override
	protected void onDestroy() {
		FrameParam app = (FrameParam) this.getApplication();
		if (app.mBMapManager != null) {
			app.mBMapManager.destroy();
			app.mBMapManager = null;
		}
		super.onDestroy();
	}
}