package com.android.bashi;

import android.location.Location;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.LocationListener;
import com.baidu.mapapi.MKAddrInfo;
import com.baidu.mapapi.MKBusLineResult;
import com.baidu.mapapi.MKDrivingRouteResult;
import com.baidu.mapapi.MKLocationManager;
import com.baidu.mapapi.MKPoiResult;
import com.baidu.mapapi.MKSearch;
import com.baidu.mapapi.MKSearchListener;
import com.baidu.mapapi.MKSuggestionResult;
import com.baidu.mapapi.MKTransitRouteResult;
import com.baidu.mapapi.MKWalkingRouteResult;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.Overlay;

public class BMap_GestureView extends MapActivity implements OnClickListener {
	BMapManager mBMapMan;
	MapView mMapView;
	MapController mMapController;
	LocationListener mLocationListener;
	GeoPoint pointlocation;
	boolean mylocation = true;
	MKSearch mMKSearch;
	MKLocationManager mLocationManager;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		findByallIdView();
		mBMapMan = new BMapManager(getApplication());
		mBMapMan.init("285B415EBAB2A92293E85502150ADA7F03C777C4", null);
		super.initMapActivity(mBMapMan);

		// mMapView.setBuiltInZoomControls(true); // 设置启用内置的缩放控件
		mMapView.getOverlays().add(new GestureOverlay());

		mMapController = mMapView.getController(); // 得到mMapView的控制权,可以用它控制和驱动平移和缩放
		GeoPoint point = new GeoPoint((int) (39.915 * 1E6),
				(int) (116.404 * 1E6)); // 用给定的经纬度构造一个GeoPoint，单位是微度 (度 //
										// 1E6)
		mMapController.setCenter(point); // 设置地图中心点
		mMapController.setZoom(14); // 设置地图zoom级别
		mBMapMan.start();
		mLocationManager = mBMapMan.getLocationManager();
		mMKSearch = new MKSearch();
		mMKSearch.init(mBMapMan, new MyMKSearchListener());

		mLocationListener = new LocationListener() {

			@Override
			public void onLocationChanged(Location location) {
				if (location != null) {
					pointlocation = new GeoPoint(
							(int) (location.getLatitude() * 1e6),
							(int) (location.getLongitude() * 1e6));
					if (mylocation) {

						mMapView.getController().animateTo(pointlocation);
						// mMKSearch.reverseGeocode(pointlocation);
						mylocation = false;
					}
				}
			}
		};
	}

	Button zoomin, zoomout, faas, showButton;

	private void findByallIdView() {
		mMapView = (MapView) findViewById(R.id.bmapsView);
		zoomin = (Button) findViewById(R.id.zoomin);
		zoomout = (Button) findViewById(R.id.zoomout);
		faas = (Button) findViewById(R.id.faas);
		zoomin.setOnClickListener(this);
		zoomout.setOnClickListener(this);
		faas.setOnClickListener(this);

	}

	int i = 12;
	boolean bTraffic = false;

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (null != v) {
			if (v == faas) {
				if (bTraffic == true) {
					mMapView.setTraffic(false);
					bTraffic = false;
				} else {
					mMapView.setTraffic(true);
					bTraffic = true;
				}

			}
			i = mMapView.getZoomLevel();
			if (v == zoomin) {

				// System.out.println(mMapView.getMaxZoomLevel());
				// System.out.println(i);

				if (i < 19 && i > 3) {
					i = i - 1;
					mMapController.setZoom(i); // 设置地图zoom级别
				} else {
					Toast.makeText(getApplicationContext(), "已经是最小级别缩小", 1)
							.show();
				}
			} else if (v == zoomout) {

				if (i < 17 && i > 2) {
					i = i + 1;

					// mMapView.setAnimation(animation) 应该可以试想一些动画效果的
					mMapController.setZoom(i); // 设置地图zoom级别
				} else {
					Toast.makeText(getApplicationContext(), "已经是最大级别缩小", 1)
							.show();
				}
			}
		}
	}

	@Override
	protected void onDestroy() {
		if (mBMapMan != null) {
			mBMapMan.destroy();
			mBMapMan = null;
		}
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		if (mBMapMan != null) {
			mBMapMan.stop();
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (mBMapMan != null) {
			mBMapMan.start();
			mLocationManager.requestLocationUpdates(mLocationListener);
		}
		super.onResume();
	}

	View popView;
	MapView.LayoutParams geoLP;

	class GestureOverlay extends Overlay implements OnGestureListener {
		private GestureDetector gestureScanner = new GestureDetector(this);
		GeoPoint point;

		public GestureOverlay() {
			mMapController = mMapView.getController();
		}

		@Override
		public boolean onDown(MotionEvent e) {
			return false;
		}

		@Override
		public void onShowPress(MotionEvent e) {

		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			return false;
		}

		@Override
		public boolean onKeyDown(int arg0, KeyEvent arg1, MapView arg2) {
			popView.setVisibility(View.INVISIBLE);
			return super.onKeyDown(arg0, arg1, arg2);
		}

		@Override
		public void onLongPress(MotionEvent e) {
			longPressEvent(e, point);
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			int distance = (int) Math.abs(e1.getY() - e2.getY());
			Toast.makeText(getApplicationContext(), distance + "", 5000).show();
			return false;
		}

		@Override
		public boolean onTouchEvent(MotionEvent arg0, MapView arg1) {
			return gestureScanner.onTouchEvent(arg0);
		}
	}

	class MyMKSearchListener implements MKSearchListener {
		
		@Override
		public void onGetAddrResult(MKAddrInfo arg0, int arg1) {

			// TODO Auto-generated method stub
			if (arg1 == 100) {
				showButton.setText("地址正确，暂无法查询");
				return;
			}
			if (arg0 == null) {
				showButton.setText("查不到此地信息");
				popView.setVisibility(View.INVISIBLE);
				return;
			}

			if (arg0 != null) {
				showButton.setText(arg0.strAddr);
			}
		}

		@Override
		public void onGetBusDetailResult(MKBusLineResult arg0, int arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onGetDrivingRouteResult(MKDrivingRouteResult arg0, int arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onGetPoiResult(MKPoiResult arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onGetSuggestionResult(MKSuggestionResult arg0, int arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onGetTransitRouteResult(MKTransitRouteResult arg0, int arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onGetWalkingRouteResult(MKWalkingRouteResult arg0, int arg1) {
			// TODO Auto-generated method stub

		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @author lilin
	 * @date 2012-11-22 下午12:55:58
	 * @annotation
	 */
	public void longPressEvent(MotionEvent e, GeoPoint point) {
		if (popView != null) {
			popView.setVisibility(View.INVISIBLE);
		}
		float x = e.getX();
		float y = e.getY();

		point = mMapView.getProjection().fromPixels((int) x, (int) y);
		popView = getLayoutInflater().inflate(R.layout.pop, null);
		mMapView.addView(popView, new MapView.LayoutParams(
				MapView.LayoutParams.WRAP_CONTENT,
				MapView.LayoutParams.WRAP_CONTENT, null,
				MapView.LayoutParams.BOTTOM_CENTER));
		popView.setVisibility(View.GONE);

		geoLP = (MapView.LayoutParams) popView.getLayoutParams();

		showButton = (Button) popView.findViewById(R.id.loca);
		showButton.setText("正在搜索中...");
		geoLP.point = point;
		mMKSearch.reverseGeocode(point);
		mMapView.invalidate();
		mMapView.updateViewLayout(popView, geoLP);
		// mapView.getController().animateTo(p);
		popView.setVisibility(View.VISIBLE);

	}

}